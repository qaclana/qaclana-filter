/*
 * Copyright 2017 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.filter;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import io.qaclana.api.Configuration;
import io.qaclana.server.proto.Empty;
import io.qaclana.server.proto.Request;
import io.qaclana.server.proto.RequestServiceGrpc;

/**
 * @author Juraci Paixão Kröhling
 */
public class RequestPublisher {
    private static final Logger logger = Logger.getLogger(RequestPublisher.class.getName());
    public static final String THREAD_POOL_SIZE_PROPERTY = "qaclana.filter.request.retry.threadpool.size";

    private ManagedChannel channel;
    private RequestServiceGrpc.RequestServiceStub service;
    private ScheduledExecutorService executorService = Executors.newScheduledThreadPool(
            Integer.parseInt(System.getProperty(THREAD_POOL_SIZE_PROPERTY, 5 + ""))
    );

    public RequestPublisher() {
        this(
                System.getProperty(Configuration.SERVER_HOSTNAME, "localhost"),
                Integer.parseInt(System.getProperty(Configuration.SERVER_RPC_PORT, "10080")),
                Boolean.parseBoolean(System.getProperty(Configuration.SERVER_RPC_USE_PLAINTEXT, "true"))
        );
    }

    public RequestPublisher(String remoteServerAddress, int remoteServerPort, boolean usePlaintext) {
        // TODO: check whether gRPC will create a new connection per object
        ManagedChannelBuilder channelBuilder = ManagedChannelBuilder
                .forAddress(remoteServerAddress, remoteServerPort)
                .usePlaintext(usePlaintext);
        channel = channelBuilder.build();
        service = RequestServiceGrpc.newStub(channel);
    }

    public void publish(Request request) {
        publish(request, 1);
    }

    public void publish(Request request, int attempt) {
        StreamObserver<Empty> observer = new StreamObserver<Empty>() {
            @Override public void onError(Throwable t) {
                // TODO: while this works fine for now, we might want to revisit and build a more robust and generic(?) retry mechanism
                if (attempt <= 5) {
                    logger.finest(() -> String.format("Failed to publish request [%s], attempt [%d]. Scheduling next.", request.getId(), attempt));
                    t.printStackTrace();
                    executorService.schedule(() -> new RequestPublisher().publish(request, attempt+1), 100, TimeUnit.MILLISECONDS);
                } else {
                    t.printStackTrace();
                    logger.warning(() -> String.format("Could not publish request [%s] after 5 attempts. Giving up.", request.getId()));
                }
            }

            @Override public void onNext(Empty value) {}
            @Override public void onCompleted() {
                logger.finest(() -> String.format("Published request [%s], attempt [%d].", request.getId(), attempt));
            }
        };

        service.process(request, observer);
    }
}
