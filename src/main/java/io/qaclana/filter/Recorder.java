/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.filter;

import java.util.logging.Logger;

import io.qaclana.server.proto.MultiValue;
import io.qaclana.server.proto.Request;
import io.undertow.server.HttpServerExchange;

/**
 * @author Juraci Paixão Kröhling
 */
public class Recorder {
    private static final Logger logger = Logger.getLogger(Recorder.class.getName());

    public void record(HttpServerExchange exchange) {
        String requestId = exchange.getRequestHeaders().get(Firewall.HTTP_HEADER_REQUEST_ID).getFirst();
        logger.finest(() -> String.format("Sending request [%s] to publisher", requestId));

        Request.Builder requestBuilder = Request.newBuilder();
        requestBuilder.setId(requestId);

        exchange.getRequestHeaders().forEach(key -> {
            MultiValue.Builder multiValueBuilder = MultiValue.newBuilder();
            key.forEach(multiValueBuilder::addValue);
            requestBuilder.putHeaders(key.getHeaderName().toString(), multiValueBuilder.build());
        });

        new RequestPublisher().publish(requestBuilder.build());
    }
}
