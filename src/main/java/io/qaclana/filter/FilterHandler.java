/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.filter;

import java.util.UUID;
import java.util.logging.Logger;

import io.qaclana.api.FirewallOutcome;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

/**
 * @author Juraci Paixão Kröhling
 */
public final class FilterHandler implements HttpHandler {
    private static final Logger logger = Logger.getLogger(FilterHandler.class.getName());
    private final HttpHandler next;
    private final Firewall firewall = new Firewall();

    public FilterHandler(final HttpHandler next) {
        if (null == next) {
            throw new IllegalArgumentException("Unable to determine the proxy handler for our filter. Giving up.");
        }
        this.next = next;
    }

    @Override
    public final void handleRequest(final HttpServerExchange exchange) throws Exception {
        final String requestId = UUID.randomUUID().toString();
        logger.fine(() -> String.format("Started processing request [%s].", requestId));

        // we add the request ID to the request, for consumption by the downstream server
        exchange.getRequestHeaders().put(Firewall.HTTP_HEADER_REQUEST_ID, requestId);
        SystemState systemState = SystemStateService.getCurrentSystemState();

        try {
            switch (systemState) {
                case DISABLED:
                    logger.finest("The firewall is currently disabled. Relaying the request as it is.");
                    next.handleRequest(exchange);
                    return;
                case PERMISSIVE:
                    logger.finest("The firewall is currently in permissive mode. Relaying the request as it is.");
                    firewall.doRequest(exchange);
                    next.handleRequest(exchange);
                    firewall.doResponse(exchange);
                case ENFORCING:
                    FirewallOutcome outcome = firewall.doRequest(exchange);
                    if (!FirewallOutcome.REJECT.equals(outcome)) {
                        logger.finest(() -> String.format("The request [%s] has been approved for further processing.", requestId));
                        next.handleRequest(exchange);
                        outcome = firewall.doResponse(exchange);
                        if (FirewallOutcome.REJECT.equals(outcome)) {
                            logger.finest(() -> String.format("The response for the request [%s] has NOT been approved.", requestId));
                            // TODO: reject the request!
                            return;
                        }
                    } else {
                        logger.finest(() -> String.format("The request [%s] has NOT been approved.", requestId));
                        // TODO: reject the request!
                        return;
                    }
            }
        } catch (Exception e) {
            logger.fine(() -> String.format("An exception occurred while processing the request. Request ID: [%s].", requestId));
            e.printStackTrace();
        }

        // and we add the request ID to the response, for consumption by the client
        exchange.getResponseHeaders().put(Firewall.HTTP_HEADER_REQUEST_ID, requestId);

        logger.fine(() -> String.format("Finished processing request [%s].", requestId));
    }
}
