/*
 * Copyright 2016 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ServiceLoader;
import java.util.UUID;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.logging.Logger;

import io.qaclana.api.FirewallOutcome;
import io.qaclana.api.spi.Processor;
import io.qaclana.processor.whitelist.WhitelistProcessor;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;

/**
 * @author Juraci Paixão Kröhling
 */
public class Firewall {
    private static final Logger logger = Logger.getLogger(Firewall.class.getName());
    public static final HttpString HTTP_HEADER_REQUEST_ID = new HttpString("Firewall-RequestID");
    private final Executor executor = Executors.newCachedThreadPool();
    private final Recorder recorder = new Recorder();

    /**
     * A list of all processors known to us.
     */
    private final List<Processor> processors;

    public Firewall(List<Processor> processors) {
        if (null != processors) {
            this.processors = new ArrayList<>(processors.size());
            processors.forEach(this::addProcessor);
        } else {
            this.processors = Collections.emptyList();
        }
    }

    public Firewall() {
        processors = new ArrayList<>();
        ServiceLoader<Processor> serviceLoader = ServiceLoader.load(Processor.class);
        serviceLoader.forEach(this::addProcessor);
    }

    private void addProcessor(Processor processor) {
        logger.finest(() -> String.format("Found processor [%s].", processor.getClass().getName()));
        this.processors.add(processor);
    }

    /**
     * Sends the exchange to all processors, collecting the answers and calculating a final {@link FirewallOutcome}
     * <p>
     * There are two phases where this method is called: request and response. During the request phase, the response is
     * null. The request can be changed by the processors during this phase. During the response phase, we expect
     * both the request and response to be provided, but the request cannot be changed. The response can be changed.
     * <p>
     * Before passing the request/response to the processors, an UUID is generated for the request. This UUID is then
     * added as a request attribute, under the name {@link #HTTP_HEADER_REQUEST_ID}. The request/response is then
     * recorded via {@link Recorder}.
     * <p>
     * Each processor gets its own job that is submitted to a {@link ExecutorCompletionService}.
     * <p>
     * At the current implementation, the processing is finished once the first {@link FirewallOutcome#REJECT} is seen
     * and pending processors are cancelled.
     *
     * @param exchange the HTTP exchange to process
     * @return the outcome for the firewall processing
     */
    private FirewallOutcome doProcess(HttpServerExchange exchange, Function<Processor, FirewallOutcome> function) {
        if (null == exchange.getRequestHeaders().get(HTTP_HEADER_REQUEST_ID)) {
            logger.finest("Request ID couldn't be found on the exchange, but one was expected to exist by now. Creating one.");
            exchange.getRequestHeaders().add(HTTP_HEADER_REQUEST_ID, UUID.randomUUID().toString());
        }

        CompletionService<FirewallOutcome> completionService = new ExecutorCompletionService<>(executor);
        List<Future<FirewallOutcome>> listOfFutureOutcomes = new ArrayList<>(processors.size());
        FirewallOutcome outcome = FirewallOutcome.NEUTRAL; // if no processors reject the request, we accept it

        try {
            processors.forEach(processor -> listOfFutureOutcomes.add(completionService.submit(() -> function.apply(processor))));

            for (int i = 0; i < processors.size(); i++) {
                final Processor processor = processors.get(i);
                try {
                    outcome = completionService.take().get();
                    if (FirewallOutcome.REJECT.equals(outcome)) {
                        logger.finest(() -> String.format("Got an affirmative rejection from a processor: [%s]", processor.getClass().getName()));
                        break;
                    }

                    if (FirewallOutcome.ACCEPT.equals(outcome)) {
                        logger.finest(() -> String.format("Got an affirmative acceptance from a processor: [%s]", processor.getClass().getName()));
                        break;
                    }
                } catch (InterruptedException | ExecutionException ignored) {
                }
            }
        } finally {
            // the currently running tasks should be fast enough, so, we let them finish whenever they are done
            // but we move on... it would be more expensive to try and cancel them just to get an occasional exception
            listOfFutureOutcomes.forEach(future -> future.cancel(false));
        }
        return outcome;
    }

    public FirewallOutcome doRequest(HttpServerExchange exchange) {
        recorder.record(exchange);
        final FirewallOutcome outcome = doProcess(exchange, (processor) -> processor.onRequest(exchange));
        logger.finest(() -> String.format("Request outcome for the Request ID [%s]: %s", exchange.getRequestHeaders().get(HTTP_HEADER_REQUEST_ID).getFirst(), outcome));
        return outcome;
    }

    public FirewallOutcome doResponse(HttpServerExchange exchange) {
        final FirewallOutcome outcome = doProcess(exchange, (processor) -> processor.onResponse(exchange));
        logger.finest(() -> String.format("Response outcome for the Request ID [%s]: %s", exchange.getRequestHeaders().get(HTTP_HEADER_REQUEST_ID).getFirst(), outcome));
        return outcome;
    }
}
