/*
 * Copyright 2017 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.filter.publisher;

import java.io.IOException;
import java.util.function.Function;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import io.qaclana.server.proto.Empty;
import io.qaclana.server.proto.Request;
import io.qaclana.server.proto.RequestServiceGrpc;

/**
 * @author Juraci Paixão Kröhling
 */
public class MockRemoteServer {
    public Server start(Function<Request, Void> function) {
        try {
            return ServerBuilder.forPort(0)
                    .addService(new MockRequestService(function))
                    .build()
                    .start();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    class MockRequestService extends RequestServiceGrpc.RequestServiceImplBase {
        private Function<Request, Void> function;
        public MockRequestService(Function<Request, Void> function) {
            this.function = function;
        }

        @Override
        public void process(Request request, StreamObserver<Empty> responseObserver) {
            try {
                function.apply(request);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                responseObserver.onNext(Empty.getDefaultInstance());
                responseObserver.onCompleted();
            }
        }
    }
}
