/*
 * Copyright 2017 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.filter.publisher;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.Test;

import io.grpc.Server;
import io.qaclana.api.Configuration;
import io.qaclana.filter.Firewall;
import io.qaclana.filter.Recorder;
import io.qaclana.server.proto.Request;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HeaderMap;
import io.undertow.util.HttpString;

/**
 * @author Juraci Paixão Kröhling
 */
public class RequestRecorderTest {

    @Test
    public void incomingRequestIsPublished() throws InterruptedException {
        // prepare
        HeaderMap headerMap = new HeaderMap();
        UUID requestId = UUID.randomUUID();
        headerMap.put(Firewall.HTTP_HEADER_REQUEST_ID, requestId.toString());
        headerMap.put(new HttpString("some-header"), "some-value");
        HttpServerExchange exchange = new HttpServerExchange(null, headerMap, null, 0);

        CountDownLatch latch = new CountDownLatch(1);
        AtomicReference<Request> requestReference = new AtomicReference<>();
        Server server = new MockRemoteServer().start((request) -> {
            requestReference.set(request);
            latch.countDown();
            return null;
        });
        System.setProperty(Configuration.SERVER_RPC_PORT, "" + server.getPort());

        // test
        new Recorder().record(exchange);

        // verify
        latch.await(1, TimeUnit.SECONDS);
        Request request = requestReference.get();
        assertNotNull(request);
        assertTrue(request.containsHeaders(Firewall.HTTP_HEADER_REQUEST_ID.toString()));
        assertEquals(2, request.getHeadersCount());
        assertEquals(request.getHeadersOrThrow(Firewall.HTTP_HEADER_REQUEST_ID.toString()).getValue(0), requestId.toString());

        // tear down
        server.shutdown();
    }

    @Test
    public void retryOnFailure() throws InterruptedException {
        // prepare
        HeaderMap headerMap = new HeaderMap();
        UUID requestId = UUID.randomUUID();
        headerMap.put(Firewall.HTTP_HEADER_REQUEST_ID, requestId.toString());
        headerMap.put(new HttpString("some-header"), "some-value");
        HttpServerExchange exchange = new HttpServerExchange(null, headerMap, null, 0);

        // test
        new Recorder().record(exchange);

        CountDownLatch latch = new CountDownLatch(1);
        AtomicReference<Request> requestReference = new AtomicReference<>();
        Server server = new MockRemoteServer().start((request) -> {
            requestReference.set(request);
            latch.countDown();
            return null;
        });
        System.setProperty(Configuration.SERVER_RPC_PORT, "" + server.getPort());

        // verify
        latch.await(1, TimeUnit.SECONDS);
        Request request = requestReference.get();
        assertNotNull(request);
        assertTrue(request.containsHeaders(Firewall.HTTP_HEADER_REQUEST_ID.toString()));
        assertEquals(2, request.getHeadersCount());
        assertEquals(request.getHeadersOrThrow(Firewall.HTTP_HEADER_REQUEST_ID.toString()).getValue(0), requestId.toString());

        // tear down
        server.shutdown();
    }
}
